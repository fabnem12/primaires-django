from django.contrib.auth import login, logout, models
from django.http import HttpResponse, Http404#, Http403
from django.shortcuts import render, redirect
from django.views.decorators.cache import cache_page

from random import choice
import os, sys
import pickle

from .forms import Initialisation, forms, TourVote, ChercheBureau, Inscription
from .lib import run, outils
from .libDjango import dossierUser, creeDossierUser, fin

sys.path.append(os.path.dirname(outils.cheminDossierLib)) #pour que pickle retrouve la librairie

def home(request):
    user = request.user
    form = Inscription(request.POST or None)

    if not user.is_authenticated and form.is_valid():
        loginStr = form.cleaned_data["login"]
        mdp = form.cleaned_data["mdp"]
        confirmation = form.cleaned_data["confirmation"]

        if mdp == confirmation:
            #on crée le compte
            user = models.User.objects.create_user(loginStr, loginStr, mdp)
            user.save()

            login(request, user)

            return redirect('/')
    else:
        aUneSimulation = os.path.exists(os.path.join(dossierUser(user)))

    return render(request, 'utilitaire/index.htm', locals())

def init(request):
    """Page d'initialisation d'une simulation"""

    form = Initialisation(request.POST or None)
    user = request.user

    if form.is_valid() and user.is_authenticated:
        dossierPerso = dossierUser(user)

        #on récupère les infos
        nbCreneaux = form.cleaned_data['nbCreneaux']
        nomsCandidats = form.cleaned_data['candidatsParticipants']
        winnerTakesAll = form.cleaned_data['winnerTakesAll']

        #on prépare la liste des candidats participants et le calendrier
        candidats = outils.posCandidats()
        calendrier = outils.calendrierAlea(nbCreneaux)

        dossierPerso = creeDossierUser(user)

        #on prépare la simulation avec une copie des objets département
        dpts = pickle.load(open(os.path.join(outils.cheminDossierLib, "dpts.p"), "rb"))

        for numDpt, dpt in dpts.items():
            pickle.dump(dpt, open(os.path.join(dossierPerso, numDpt+".p"), "wb"))

        #pour le calendrier, on stocke un tuple, le calendrier lui-même et un entier représentant le créneau de vote en cours
        pickle.dump((calendrier, 0), open(os.path.join(dossierPerso, "calendar.p"), "wb"))

        #on stocke les candidats dans un fichier pickle, pour pouvoir les retirer à tout moment de la simulation
        pickle.dump(candidats, open(os.path.join(dossierPerso, "candidats.p"), "wb"))

        #la liste des numéros des départements ayant voté
        pickle.dump(set(), open(os.path.join(dossierPerso, "dptsAyantVote.p"), "wb"))

        #la liste des bureaux ayant voté
        pickle.dump([], open(os.path.join(dossierPerso, "listeBureaux.p"), "wb"))

        #winner-takes-all
        pickle.dump(winnerTakesAll, open(os.path.join(dossierPerso, "winnerTakesAll.p"), 'wb'))

        #résultats : derniersAlignements et délégués cumulés, et gagnant par département
        derniersAlignements, delegues = dict(), dict()
        pickle.dump((derniersAlignements, delegues), open(os.path.join(dossierPerso, "resultatsCumules.p"), "wb"))

        dptsParGagnants = dict()
        for candidat in candidats: dptsParGagnants[candidat] = []
        pickle.dump(dptsParGagnants, open(os.path.join(dossierPerso, "dptsParGagnants.p"), "wb"))

        return redirect('/vote')
    elif not user.is_authenticated:
        return redirect('/')

    return render(request, "utilitaire/init.htm", locals())

def deconnexion(request):
    logout(request)
    return redirect('/')

def resultatsDpt(request, numDpt):
    """
    Vue pour afficher les résultats d'un département:
        - carte avec un point par bureau de vote, de la couleur du candidat arrivé en tête
        - totaux de premier et dernier alignement
        - résumé des représentants et délégués du département
    """

    user = request.user
    if not user.is_authenticated or not os.path.exists(dossierUser(user)):
        return redirect('/') #un utilisateur pas authentifié ne doit pas accéder aux résultats d'un autre

    cheminPerso, dossierPerso = dossierUser(user), dossierUser(user, nomOnly=True)
    nomFichier = numDpt+".svg"
    nomDpt = "de "+outils.nomsDpts[numDpt]

    #on génère le fichier .svg si le dpt a voté
    dpt = pickle.load(open(os.path.join(cheminPerso, numDpt+".p"), "rb"))
    if dpt.dernierAlignement != dict():# and not os.path.exists(os.path.join(cheminPerso, nomFichier)): #le dpt a voté, on génère son fichier svg
        outils.carteDpt(dpt, cheminPerso)

    fichierExiste = os.path.exists(os.path.join(cheminPerso, nomFichier))

    return render(request, "utilitaire/resultatsAll.htm", locals())

def resultatsTour(request):
    """
    Afficher les résultats du tour
    """

    user = request.user
    if not user.is_authenticated or not os.path.exists(dossierUser(user)):
        return redirect("/")

    cheminPerso, dossierPerso = dossierUser(user), dossierUser(user, nomOnly=True)
    _, idTour = pickle.load(open(os.path.join(cheminPerso, "calendar.p"), "rb"))

    nomFichier = "resultatsTour.svg"
    nomDpt = "du tour {}".format(idTour)

    fichierExiste = os.path.exists(os.path.join(cheminPerso, nomFichier))

    return render(request, "utilitaire/resultatsAll.htm", locals())

@cache_page(15)
def resultatsAll(request):
    """
    Vue pour afficher les résultats de toute la France (avec seulement les départements ayant voté):
        - carte avec les départements colorés en fonction du candidat arrivé en tête en fonction des représentants
        - totaux de dernier alignement et délégués
    """

    user = request.user
    if not user.is_authenticated or not os.path.exists(dossierUser(user)):
        return redirect('/') #il faut être connecté pour voir les résultats de la simulation

    cheminPerso, dossierPerso = dossierUser(user), dossierUser(user, nomOnly=True)
    nomFichier = "resultatsAll.svg"
    nomDpt = "globaux"

    fichierExiste = os.path.exists(os.path.join(cheminPerso, nomFichier))

    return render(request, "utilitaire/resultatsAll.htm", locals())

def choixBureau(request, recherche = "", tousCandidats = ""):
    """
    Vue pour accéder aux résultats d'un bureau de vote en particulier
    """

    user = request.user
    if not user.is_authenticated or not os.path.exists(dossierUser(user)):
        return redirect('/')
    dossierPerso = dossierUser(user)

    form = ChercheBureau(request.GET or None)
    if form.is_valid():
        nomBureau = form.cleaned_data["nomBureau"]
        return redirect('/resultatsBureau/'+nomBureau)

    if "²" in recherche:
        nom, numDpt = recherche.split("²")
        dpt = pickle.load(open(os.path.join(dossierPerso, numDpt+".p"), "rb"))

        correspondants = [(x.nom, dpt.num, dpt.nom) for x in dpt if nom.lower() == x.nom.lower()]
        del dpt
    else:
        bureauxAyantVote = pickle.load(open(os.path.join(dossierPerso, "listeBureaux.p"), "rb"))

        if "random" in recherche:
            nomBureau, numDpt, _ = choice(bureauxAyantVote)
            return redirect('/resultatsBureau/'+nomBureau+'²'+numDpt+"/"+tousCandidats)
        else:
            correspondants = [] if recherche == "" else sorted([x for x in bureauxAyantVote if recherche.lower() in x[0].lower() or recherche.lower() in x[1].lower() or recherche.lower() in x[2].lower()], key=lambda x: x[0])
            del bureauxAyantVote

    if len(correspondants) != 1:
        nbCorrespondants = len(correspondants)

        return render(request, 'utilitaire/choixBureau.htm', locals())
    else:
        nomBureau, numDpt, nomDpt = correspondants[0]
        dpt = pickle.load(open(os.path.join(dossierPerso, numDpt+".p"), "rb"))
        bureau = dpt[nomBureau]

        if tousCandidats == "&": #paramètre secret pour afficher les résultats du bureau s'il avait pu voter pour tous les candidats
            bureau.voteNew(outils.posCandidats())

        del dpt

        cheminPerso, dossierPerso = dossierUser(user), dossierUser(user, True)
        nomFichier = bureau.nom+".svg"

        codeSvgBureau = outils.derouleBureauVote(bureau)
        with open(os.path.join(cheminPerso, nomFichier), "w") as f:
            f.write(codeSvgBureau)

        nomDpt = "à {} ({})".format(bureau.nom, nomDpt)
        fichierExiste = True
        return render(request, 'utilitaire/resultatsAll.htm', locals())

def choixDpt(request):
    """
    Vue pour choisir de quel département voir les résultats.
    """

    user = request.user
    if not user.is_authenticated or not os.path.exists(dossierUser(user)):
        return redirect('/')
    dossierPerso = dossierUser(user)

    calendrier, idTour = pickle.load(open(os.path.join(dossierPerso, "calendar.p"), "rb"))
    infosCal = [(index+1, [(numDpt, outils.nomsDpts[numDpt], index>=idTour) for numDpt in sorted(fournee)]) for index, fournee in enumerate(calendrier)]

    #on vide les fichiers svg des dpts pour libérer un peu de mémoire
    for numDpt in outils.listeDpts:
        with open(os.path.join(dossierPerso, numDpt+".svg"), "w") as f:
            f.write("")

    return render(request, "utilitaire/resultatsParDpt.htm", locals())

def redir(request, lien="/"):
    """
    Vue pour réinitialiser la procédure.
    """

    return render(request, "utilitaire/init.htm", locals())

def calculs(request):
    """
    "Vue" pour lancer les calculs des votes pour les départements en cours
    """

    user = request.user
    if not user.is_authenticated or not os.path.exists(dossierUser(user)):
        return redirect('/')

    dossierPerso, nomDossierPerso = dossierUser(user), dossierUser(user, True)
    candidats = pickle.load(open(os.path.join(dossierPerso, "candidats.p"), "rb"))

    form = TourVote(candidats)(request.POST or None)

    #récupération des départements à voter
    calendar, idTour = pickle.load(open(os.path.join(dossierPerso, "calendar.p"), "rb"))
    if idTour == len(calendar): #le dernier tour a été fait, on peut passer directement aux résultats
        if not os.path.exists(os.path.join(dossierPerso, "tout.p")):
            #fin(dossierPerso)
            pass

        return redirect('/resultats')
    numDptsAVoter = calendar[idTour]

    if form.is_valid(): #on peut lancer le vote
        #on récupère la liste de tous les bureaux qui ont voté jusque là, pour y rajouter celle des bureaux votant dans le tour en cours
        listeBureaux = pickle.load(open(os.path.join(dossierPerso, "listeBureaux.p"), "rb"))

        #on retrouve si la simulation est en "winner-takes-all" ou pas
        winnerTakesAll = os.path.exists(os.path.join(dossierPerso, "winnerTakesAll.p")) and pickle.load(open(os.path.join(dossierPerso, "winnerTakesAll.p"), "rb"))

        #on récupère les noms des candidats restants et les objets correspondants
        candidatsInit = outils.posCandidats(False)
        candidatsNew = [candidatsInit[x] for x in form.cleaned_data["candidatsParticipants"]]

        pickle.dump(candidatsNew, open(os.path.join(dossierPerso, "candidats.p"), "wb"))

        #on met à jour la liste des numéros des départements ayant voté
        numDptsAyantVote = pickle.load(open(os.path.join(dossierPerso, "dptsAyantVote.p"), "rb"))
        for numDpt in numDptsAVoter: numDptsAyantVote.add(numDpt)
        pickle.dump(numDptsAyantVote, open(os.path.join(dossierPerso, "dptsAyantVote.p"), "wb"))

        #on retrouve les objets des départements à voter
        dptsAVoter = [pickle.load(open(os.path.join(dossierPerso, numDpt+".p"), "rb")) for numDpt in numDptsAVoter]

        #on prépare la mise à jour le résumé des derniersAlignements et délégués
        dptsParGagnants = pickle.load(open(os.path.join(dossierPerso, "dptsParGagnants.p"), "rb"))

        derniersAlignementsParDptTour = dict()
        deleguesParDptTour = dict()

        dptsParGagnantsTour = dict()
        for candidat in candidats: dptsParGagnantsTour[candidat] = []

        #on les fait voter
        for dpt in dptsAVoter:
            dernierAlignementDpt, deleguesDpt = dpt.vote(candidatsNew, winnerTakesAll)
            pickle.dump(dpt, open(os.path.join(dossierPerso, dpt.num+".p"), "wb")) #sauvegarde du dpt
            listeBureaux += [(bureau.nom, dpt.num, dpt.nom) for bureau in dpt.bureaux]
            gagnant = dpt.gagnant()

            derniersAlignementsParDptTour[dpt.num] = dernierAlignementDpt
            deleguesParDptTour[dpt.num] = deleguesDpt

            dptsParGagnantsTour[gagnant].append(dpt.num)

            del dpt
        del dptsAVoter

        #on met à jour la liste des bureaux
        pickle.dump(listeBureaux, open(os.path.join(dossierPerso, "listeBureaux.p"), "wb"))
        del listeBureaux

        #on met à jour le résumé des derniersAlignements et délégués
        derniersAlignementsParDpt, deleguesParDpt = pickle.load(open(os.path.join(dossierPerso, "resultatsCumules.p"), "rb"))
        derniersAlignementsParDpt.update(derniersAlignementsParDptTour)
        deleguesParDpt.update(deleguesParDptTour)

        for candidat, dptsGagnes in dptsParGagnantsTour.items():
            dptsParGagnants[candidat] += dptsGagnes

        pickle.dump((derniersAlignementsParDpt, deleguesParDpt), open(os.path.join(dossierPerso, "resultatsCumules.p"), "wb"))
        pickle.dump(dptsParGagnants, open(os.path.join(dossierPerso, "dptsParGagnants.p"), "wb"))

        #on met à jour l'id du tour
        pickle.dump((calendar, idTour+1), open(os.path.join(dossierPerso, "calendar.p"), "wb"))

        #on fait la carte résumé du tour
        codeSVGTour = outils.carte2(derniersAlignementsParDptTour, deleguesParDptTour, dptsParGagnantsTour, candidatsNew)
        with open(os.path.join(dossierPerso, "resultatsTour.svg"), "w") as f:
            f.write(codeSVGTour)

        codeSVGTout = outils.carte2(derniersAlignementsParDpt, deleguesParDpt, dptsParGagnants, candidatsNew)
        with open(os.path.join(dossierPerso, "resultatsAll.svg"), "w") as f:
            f.write(codeSVGTout)

        del codeSVGTout
        del codeSVGTour

        if idTour+1 == len(calendar): #on a fait le dernier tour
            #fin(dossierPerso)

            return redirect('/resultats')
        else:
            return redirect('/vote')
    else:
        nbTours = len(calendar)
        idTour += 1

        #on cherche juste à générer une carte à montrer pour résumer le tour
        codeSVG = outils.cartes([], [], [numDptsAVoter])
        with open(os.path.join(dossierPerso, "recapTour.svg"), "w") as f:
            f.write(codeSVG)

        return render(request, 'utilitaire/vote.htm', locals())
