from django.shortcuts import render, redirect
from django.urls import path
from material.frontend.apps import ModuleMixin
from material.frontend.registry import modules

from . import views

def redir(lien):
    return lambda request: redirect(lien)

class Home(ModuleMixin):
    """
    Page d'accueil
    """

    name = "Home"
    label = "Tableau de bord"
    icon = "<i class='material-icons'>dashboard</i>"
    menu = ""
    order = 0

    def index_url(self):
        return "/"

    def installed(self):
        True

class Init(ModuleMixin):
    """
    Module pour lancer une nouvelle simulation
    """

    name = "Nouvelle simulation"
    label = "Nouvelle simulation"
    icon = "<i class='material-icons'>add</i>"
    menu = ""
    order = 1

    def index_url(self):
        return "/init"

    def installed(self):
        return True

class Vote(ModuleMixin):
    """
    Module pour lancer le vote
    """
    name = "Vote"
    label = "Vote"
    icon = "<i class='material-icons'>how_to_vote</i>"
    menu = ""
    order = 2

    def index_url(self):
        return '/vote'

    def installed(self):
        return True

class ResultatsTour(ModuleMixin):
    """
    Module pour voir les résultats du dernier tour effectué
    """
    name = "Résultats tour"
    label = "Résultats tour"
    icon = "<i class='material-icons'>poll</i>"
    menu = ""
    order = 3

    index_url = lambda self: "/resultatsTour"
    installed = lambda self: True

class Resultats(ModuleMixin):
    """
    Module pour voir les résultats globaux
    """
    name = "Résultats"
    label = "Résultats globaux"
    icon = "<i class='material-icons'>poll</i>"
    menu = ""
    order = 4

    def index_url(self):
        return "/resultats"

    def installed(self):
        return True

class ResultatsDpt(ModuleMixin):
    """
    Module pour voir les résultats d'un département
    """
    name = "Résultats dpt"
    label = "Résultats par département"
    icon = "<i class='material-icons'>poll</i>"
    menu = ""
    order = 5

    index_url = lambda self: "/resultatsDpt"
    installed = lambda self: True

class ResultatsBureau(ModuleMixin):
    """
    Module pour voir les résultats d'un bureau donné
    """
    name = "Résultats bureau"
    label = "Résultats par bureau"
    icon = "<i class='material-icons'>poll</i>"
    menu = ""
    order = 6

    index_url = lambda self: "/resultatsBureau/"
    installed = lambda self: True

modules.register(Home())
modules.register(Init())
modules.register(Vote())
modules.register(ResultatsTour())
modules.register(Resultats())
modules.register(ResultatsDpt())
modules.register(ResultatsBureau())

del modules._registry["viewflow_frontend"]

urlpatterns = [
    path('', views.home, name = "home"),
    path('init/', views.init, name = "init"),
    path('resultatsDpt/<str:numDpt>', views.resultatsDpt, name = "resultatsDpt"),
    path('resultats/', views.resultatsAll, name = "resultatsAll"),
    path('resultatsTour/', views.resultatsTour, name = "resultatsTour"),
    path('vote/', views.calculs, name = "vote"),
    path('redirection/<str:lien>', views.redir, name = "redir"),
    path('accounts/profile/', redir('/')),
    path('accounts/logout/', views.deconnexion, name = "deconnexion"),
    path('resultatsDpt/', views.choixDpt, name = "choixDpt"),
    path('resultatsBureau/<str:recherche>/<str:tousCandidats>', views.choixBureau, name="resBureauTout"),
    path('resultatsBureau/<str:recherche>/', views.choixBureau, name = "trouveBureau"),
    path('resultatsBureau/', views.choixBureau, name = "choixDptBureau"),
]
