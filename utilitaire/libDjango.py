import os
import pickle
from .lib import outils

#on enregistre le numéro de la simulation en cours dans le champ last_name de l'utilisateur

def dossierUser(user, nomOnly=False):
    dernier = "0" if "last_name" not in dir(user) else user.username+"-"+user.last_name

    return dernier if nomOnly else os.path.join(outils.cheminData, dernier)

def creeDossierUser(user):
    idSim = 1
    while os.path.exists(os.path.join(outils.cheminData, user.username+"-"+str(idSim))):
        idSim += 1

    user.last_name = str(idSim)
    user.save()

    dossierPerso = os.path.join(outils.cheminData, "{}-{}".format(user.username, idSim))
    os.mkdir(dossierPerso)

    return dossierPerso

def listeSims(user):
    liste = []

    dossierPerso = dossierUser(user)
    if dossierPerso:
        for nomDossier in os.listdir(os.dirname(dossierPerso)):
            if user.user_name+"-" in nomDossier:
                liste.append(int(nomDossier.split("-")[-1]))

def fin(dossierPerso):
    bureaux = []

    for numDpt in outils.listeDpts:
        dpt = pickle.load(open(os.path.join(dossierPerso, numDpt+".p"), "rb"))
        bureaux += dpt.bureaux
        del dpt

    alignements = dict()
    preferences = dict()
    for bureau in bureaux:
        #ordres de preference des votants
        for votant in bureau.votants:
            prefVotant = tuple(votant.preferences)

            if prefVotant not in preferences:
                preferences[prefVotant] = dict()
            if bureau not in preferences[prefVotant]:
                preferences[prefVotant][bureau] = 0

            preferences[prefVotant][bureau] += 1

        #alignements
        alignements[bureau] = dict()
        for candidat in bureau.premierAlignement: alignements[bureau][candidat] = 1 #on stocke les candidats présents au premier tour

        for tour, candidatsParticipants in enumerate(bureau.toursAlignements):
            for candidat in candidatsParticipants:
                alignements[bureau][candidat] += 2**tour #truc malin pour stocker avec les puissances de 2 dans quel tour chaque candidat a été

        bureau.votants = []
        bureau.premierAlignement = dict()
        bureau.dernierAlignement = dict()
        bureau.representants = dict()

    infos = (bureaux, preferences, alignements)
    pickle.dump(infos, open(os.path.join(dossierPerso, "tout.p"), "wb"))
