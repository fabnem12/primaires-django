from django.apps import AppConfig


class UtilitaireConfig(AppConfig):
    name = 'utilitaire'
