from django import forms
from material import Layout, Row

from .lib import outils

class Inscription(forms.Form):
    login = forms.CharField(label="Nom d'utilisateur")
    mdp = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)
    confirmation = forms.CharField(label="Confirmation du mot de passe", widget=forms.PasswordInput)

class Initialisation(forms.Form):
    nbCreneaux = forms.IntegerField(label="Nombre de créneaux de vote", initial=20, min_value=1, max_value=35)

    candidats = sorted(outils.posCandidats(), key=lambda x: x.nom)
    candidatsParticipants = forms.MultipleChoiceField(label="Candidats participants", widget=forms.CheckboxSelectMultiple, choices=[(x,x) for x in candidats], initial=candidats)
    winnerTakesAll = forms.BooleanField(label = "Winner-takes-all ? (le gagnant d'un département gagne tous ses délégués) (sinon répartition proportionnelle)", required = False)

def TourVote(candidats):
    class FormTourVote(forms.Form):
        candidatsParticipants = forms.MultipleChoiceField(label="Candidats participants", widget=forms.CheckboxSelectMultiple, choices=[(x, x) for x in candidats], initial=candidats)

    return FormTourVote

class ChercheBureau(forms.Form):
    nomBureau = forms.CharField(label="Nom du bureau de vote / de la ville")
