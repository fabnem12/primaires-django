from math import floor

listeDpts = list(sorted([str(x).zfill(2) for x in range(1, 19+1)] + ["2A", "2B"] + [str(x) for x in range(21, 95+1)] + ["ZA", "ZB", "ZC", "ZD", "ZZ", "ZX", "ZW", "ZM", "ZN", "ZP", "ZS"]))

class Dpt:
    def __init__(self, numDpt, bureaux, coord):
        self.num = numDpt
        self.bureaux = bureaux
        self.pop = 0#sum([len(x.votants) for x in bureaux])
        self.nom = ""
        self.coords = coord

        self.nbDelegues = 1
        self.delegues = dict()

        self.premierAlignement = dict()
        self.dernierAlignement = dict()
        self.representants = dict()

    def vote(self, candidats, winnerTakesAll=False):
        if self.delegues != dict():
            return self.dernierAlignement, self.delegues

        derniersAlignements = dict()
        premiersAlignements = dict()
        for candidat in candidats:
            derniersAlignements[candidat] = 0
            premiersAlignements[candidat] = 0

        reprDpt = dict()

        for bureau in self.bureaux:
            dernierAlignement, premierAlignement, reprBureau, _ = bureau.voteNew(candidats)

            for candidat, votes in premierAlignement.items():
                premiersAlignements[candidat] += len(votes)

                if candidat in dernierAlignement:
                    derniersAlignements[candidat] += len(dernierAlignement[candidat])

                if candidat in reprBureau:
                    if candidat not in reprDpt: reprDpt[candidat] = 0

                    reprDpt[candidat] += reprBureau[candidat]

        self.dernierAlignement = derniersAlignements
        self.premierAlignement = premiersAlignements
        self.representants = reprDpt

        return derniersAlignements, self.repartDelegues(winnerTakesAll)

    def repartDelegues(self, winnerTakesAll):
        #on répartit les délégués à la proportionnelle d'Hondt

        seuilElec = sum(self.representants.values()) / self.nbDelegues
        candidats = list(self.representants.keys())
        nbDel = dict()

        if winnerTakesAll:
            gagnant = None
            maxRepr = 0

            for candidat, nbRepr in self.representants.items():
                if nbRepr == 0: continue

                if nbRepr > maxRepr or (nbRepr == maxRepr and self.dernierAlignement[candidat] > self.dernierAlignement[gagnant]):
                    gagnant = candidat
                    maxRepr = nbRepr

            nbDel[gagnant] = self.nbDelegues
        else:
            for candidat, nbRepr in self.representants.items():
                nbDel[candidat] = floor(nbRepr / seuilElec)

            deleguesRestants = self.nbDelegues - sum(nbDel.values())
            for _ in range(deleguesRestants):
                moyMax = 0
                candidatMax = None

                for candidat, nbRepr in self.representants.items():
                    moy = nbRepr / (nbDel[candidat] + 1)

                    #soit le candidat a une meilleure moyenne, soit il a la même moyenne pour les représentants, mais a plus de derniers alignements
                    if moy > moyMax or (candidatMax is not None and moy == moyMax and self.dernierAlignement[candidat] > self.dernierAlignement[candidatMax]):
                        moyMax = moy
                        candidatMax = candidat

                nbDel[candidatMax] += 1

            for candidat, nbDelegues in list(nbDel.items()):
                if nbDelegues == 0: del nbDel[candidat]

        self.delegues = nbDel
        return nbDel

    def gagnant(self):
        return sorted(self.representants, key=lambda x: (self.representants[x], self.dernierAlignement[x]), reverse=True)[0]

    def __repr__(self):
        return "{} : {} délégués".format(self.num, self.nbDelegues)

    def __str__(self):
        return "{} : {} délégués".format(self.num, self.nbDelegues)

    def __getitem__(self, index): #pour récupérer un bureau de vote par son nom ou son index
        if type(index) == int:
            return self.bureaux[index]

        for bureau in self.bureaux:
            if index in (bureau.num, bureau.nom):
                return bureau

        raise KeyError("Je n'ai pas de bureau de vote avec cet identifiant !")

    def __contains__(self, index):
        for bureau in self.bureaux:
            if bureau.nom == index:
                return True
        return False

    def __iter__(self):
        for bureau in self.bureaux:
            yield bureau

    def reset(self):
        for bureau in self.bureaux:
            bureau.reset()
