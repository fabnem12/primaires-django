from .Votant import Votant
from math import floor
from random import shuffle, choice


class BureauVote:
    def __init__(self, num, nom, votants, nbRepresentants):
        self.num = num
        self.nom = nom
        self.votants = votants
        self.nbRepresentants = nbRepresentants
        self.seuilViabilite = max(0.15, 1 / (nbRepresentants+1)) #le seuil de viabilité est supérieur ou égal à 15%
        self.pos = []
        self.coord = []
        self.initRes()

    def __repr__(self):
        return "{} : {} votants, {} représentants".format(self.nom, self.votants if type(self.votants) == int else len(self.votants), self.nbRepresentants)

    def genereVotants(self):
        nuancesPond = [i for i in range(11) for _ in range(self.pos[i])]
        nbVotants = self.votants

        self.votants = []
        for _ in range(nbVotants):
            self.votants.append(Votant(choice(nuancesPond)))

    def initRes(self):
        self.representants = dict()
        self.premierAlignement = dict()
        self.dernierAlignement = dict()
        self.toursAlignements = []

    def voteNew(self, candidats, ):
        if type(self.votants) == int:
            self.genereVotants()
        self.initRes()

        finaux = candidats.copy()
        votes = dict()

        for candidat in candidats:
            votes[candidat] = []

        for votant in self.votants:
            prefere = votant.vote(candidats)[0]
            votes[prefere].append(votant)

        premierAlignement = dict()
        for candidat, votants in dict(votes).items():
            premierAlignement[candidat] = votants.copy()

            if len(votants) == 0:
                del votes[candidat] #on élimine d'emblée les candidats qui ont 0 voix
                finaux.remove(candidat)

        toursAlignements = []
        while len([x for x in finaux if len(votes[x]) / len(self.votants) <= self.seuilViabilite]) > 0:
            candidatElimine = self.dernier(votes, finaux)
            finaux.remove(candidatElimine)

            for votantDecu in votes[candidatElimine]:
                nouveauVote = votantDecu.vote(finaux)[0]
                votes[nouveauVote] = votes[nouveauVote] + [votantDecu]
            del votes[candidatElimine]

            resumeTour = dict(votes)
            toursAlignements.append(resumeTour)

        representants = self.siegesRepresentants(votes)

        self.premierAlignement = dict() #totaux du premier alignement
        for candidat, votants in premierAlignement.items():
            self.premierAlignement[candidat] = len(votants)
        self.dernierAlignement = dict() #totaux du dernier alignement
        for candidat, votants in votes.items():
            self.dernierAlignement[candidat] = len(votants)
        self.toursAlignements = toursAlignements

        return votes, premierAlignement, representants, toursAlignements

    def ecartPos(self, votantsCandidat):
        """
        Ecart idéologique d'un ensemble de votants par rapport au positionnement moyen du bureau de vote
        """
        moyPosCandidat = sum([x.position for x in votantsCandidat]) / len(votantsCandidat)
        moyPosBureau = sum([index*val for index, val in enumerate(self.pos)]) / sum(self.pos)

        return abs(moyPosCandidat - moyPosBureau)

    def siegesRepresentants(self, votes): #répartis à la proportionnelle d'Hondt
        candidats = list(votes.keys())
        nbVotes = len(self.votants)

        if sum(self.representants.values()) == self.nbRepresentants: #les représentants ont déjà été calculés
            return self.representants

        representants = dict()
        for candidat in candidats: representants[candidat] = max(1, round(self.nbRepresentants * len(votes[candidat]) / nbVotes))

        while sum(representants.values()) < self.nbRepresentants:
            moyMax = float("inf")
            candidatMax = None

            for candidat in candidats:
                moy = len(votes[candidat]) / (representants[candidat] + 1) #moyenne de voix par siège

                if candidatMax is None or moy > moyMax or (moy == moyMax and self.ecartPos(votes[candidat]) < self.ecartPos(votes[candidatMax])):
                    moyMax = moy
                    candidatMax = candidat

            representants[candidatMax] += 1

        self.representants = representants

        return self.representants

    def reset(self):
        if type(self.votants) == list:
            self.votants = len(self.votants)
        self.representants = dict()
        self.premierAlignement = dict()
        self.dernierAlignement = dict()


    def dernier(self, dicoVotes, candidatsATraiter):
        """trouver le dernier candidat, qui doit alors se retirer
        -1er critère : le nombre d'alignements -> le dernier est le candidat qui a le moins d'alignements en sa faveur
        -2e critère : en cas d'ex-aequo, on regarde le positionnement moyen des électeurs de chaque candidat,
        on le compare avec le positionnement moyen du bureau, et le candidat le plus éloigné de ce dernier est dernier
        -> a priori un candidat plus proche du positionnement du bureau a plus de chances de gagner qu'un autre
        -3e critère : dans le cas très peu probable de nouvel ex-aequo, le hasard tranche
        """

        mini = float("inf")
        candidatMini = None

        #on met l'ordre d'appel des candidats au hasard, on faire de facto un tirage au sort dans le cas peu probable d'ex-aequo
        candidats = candidatsATraiter.copy()
        shuffle(candidats)
        candidats.sort(key=lambda x: (len(dicoVotes[x]), -self.ecartPos(dicoVotes[x])))

        return candidats[0]

    def condorcet(self, candidats):
        """
        Donne le résultat de chaque duel de Condorcet dans le bureau de vote
        (indicatif, les résultats ne sont pas figés après calcul et peuvent potentiellement différer d'un appel à l'autre de la fonction)
        """

        duels = [(x, y) for index, x in enumerate(candidats) for y in candidats[index+1:]]
        resultats = dict()

        for duel in duels:
            candidatA, candidatB = duel
            resultats[duel] = {candidatA: [], candidatB:[]}

            for votant in self.votants:
                prefere = votant.vote((candidatA, candidatB))[0]

                resultats[duel][prefere].append(votant)

        return resultats

    def __iter__(self):
        for votant in self.votants:
            yield votant

    def __getitem__(self, index):
        if type(self.votants) == int or index >= len(self.votants):
            raise ValueError("Ce bureau n'a pas encore généré ses votants, ou vous en demandez un trop loin")

        return self.votants[index]
