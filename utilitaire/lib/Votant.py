from random import choice

class Votant:
    def __init__(self, position):
        self.position = position
        self.preferences = []
        self.pointsDeVue = dict() #stocker le positionnement ressenti de chaque candidat par le votant

    def vote(self, candidats):
        #le votant s'est déjà positionné par rapport à chaque candidat
        if self.preferences != [] and len([x for x in candidats if x not in self.pointsDeVue]) == 0:
            return [x for x in self.preferences if x in candidats]

        #on s'assure que le votant s'est positionné pour chaque candidat
        for candidat in [x for x in candidats if x not in self.pointsDeVue]:
            #positionCandidat = sum([choice(candidat.positionPond) for _ in range(5)]) / 5
            #positionnement ressenti du candidat par le votant
            #self.pointsDeVue[candidat] = positionCandidat
            self.pointsDeVue[candidat] = sum([abs(self.position - choice(candidat.positionPond))*(11**i) for i in range(3)])

        self.preferences += [x for x in candidats if x not in self.preferences] #on ajoute les candidats aux préférences du votant

        #on retrie les préférences
        #self.preferences.sort(key=lambda x: abs(self.position - self.pointsDeVue[x]))
        self.preferences.sort(key=lambda x: tuple([pointDevue for pointDevue in (self.pointsDeVue[x] % 11, (self.pointsDeVue[x] // 11) % 11, (self.pointsDeVue[x] // 121) % 11)]))

        #self.preferences inclut possiblement des candidats qui ne sont pas proposés au moment de l'appel de cette fonction, on ne garde que ceux qui sont proposés
        return [x for x in self.preferences if x in candidats]

    def prefere(self):
        return None if len(self.preferences) == 0 else self.preferences[0]
