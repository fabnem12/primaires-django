class Candidat:
    def __init__(self, nom, positionnement, couleur):
        self.nom = nom
        self.positionnement = positionnement
        self.couleur = couleur

        self.positionPond = []
        for index, prop in enumerate(self.positionnement):
            for _ in range(prop):
                self.positionPond.append(index)

        self.positionMoy = sum(self.positionPond) / len(self.positionPond)

    def __str__(self):
        return self.nom

    def __repr__(self):
        return self.nom

    def __hash__(self):
        return hash(self.nom)

    def __eq__(self, compare):
        return type(compare) == Candidat and self.nom == compare.nom
