from BureauVote import BureauVote
from outils import posCandidats, calendrier, tendances, popNew, cartes
import os
import pickle

cheminFichierLib = os.path.dirname(os.path.abspath(__file__))
cheminData = os.path.join(os.path.dirname(cheminFichierLib), "/static/utilitaire/data/")

def run(dptsVotants, derniersAlignements, delegues, representants, dptsAyantVote):
    for numDpt, dpt in dptsVotants.items():
        derniersAlignementsDpt, deleguesDpt = dpt.vote(candidats)
        
        for candidat, nbVotes in derniersAlignementsDpt.items():
            derniersAlignements[candidat] += nbVotes
        
        for candidat, nbDelegues in deleguesDpt.items():
            delegues[candidat] += nbDelegues
        
        if not os.path.exists(cheminData):
            os.mkdir(cheminData)
        
        fichierDpt = os.path.join(cheminData, numDpt+".p")
        pickle.dump(dpt, open(fichierDpt, "wb"))
    
    cartes(dptsAyantVote, derniersAlignements, delegues, candidats)

if __name__ == "__main__":
    cal = calendrier()
    print("Calendrier :")
    print(cal)
    tendancesLocales = tendances()
    bureauxVote = popNew(tendancesLocales)
    
    candidats = list(posCandidats().values())
    candidatsInit = candidats.copy()
    resTot = dict()
    representants = dict()
    bureauxAyantVote = dict()
    
    derniersAlignements = dict()
    delegues = dict()
    for candidat in candidats:
        derniersAlignements[candidat] = 0
        delegues[candidat] = 0
    
    dptsAyantVote = []
    for index, fournee in enumerate(cal):
        print("Tour", index+1)
        
        for numDpt in fournee:
            print(numDpt)
            dpt = bureauxVote[numDpt]
            
            derniersAlignementsDpt, deleguesDpt = dpt.vote(candidats)
            
            print("Derniers alignements")
            for candidat, nbVotes in derniersAlignementsDpt.items():
                derniersAlignements[candidat] += nbVotes
                print(candidat, round(100 * nbVotes / sum(derniersAlignementsDpt.values()), 3), "%")
            
            for candidat, nbDelegues in deleguesDpt.items():
                delegues[candidat] += nbDelegues
            
            dptsAyantVote.append(dpt)
            
            if "data" not in os.listdir():
                os.mkdir("data")
            pickle.dump(dpt, open("data/{}.p".format(numDpt), "wb"))
            
            print()
        
        cartes(dptsAyantVote, derniersAlignements, delegues, candidats)
        
        #on demande à retirer ou pas un candidat, sauf à la fin du dernier tour...
        if index+1 < len(cal) and "oui" in input("Retrait candidat ?\n").lower():
            for index, candidat in enumerate(candidats):
                print(index+1, candidat)
    
            numCandidat = input("N°: ")
            if numCandidat.isdigit() and 0 <= int(numCandidat)-1 < len(candidats):
                print(candidats[int(numCandidat) - 1], "s'est retiré(e) de la course")
                del candidats[int(numCandidat) - 1]
                print()