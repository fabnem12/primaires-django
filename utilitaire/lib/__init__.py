from .BureauVote import BureauVote
from .outils import *

import os
import pickle

def run(dptsVotants, candidats, derniersAlignements, delegues, dptsAyantVote):
    print("Start")
    for dpt in dptsVotants:
        numDpt = dpt.num
        
        derniersAlignementsDpt, deleguesDpt = dpt.vote(candidats)
        
        for candidat, nbVotes in derniersAlignementsDpt.items():
            derniersAlignements[candidat] += nbVotes
        
        for candidat, nbDelegues in deleguesDpt.items():
            delegues[candidat] += nbDelegues
        
        if not os.path.exists(cheminData):
            os.mkdir(cheminData)
        
        fichierDpt = os.path.join(cheminData, numDpt+".p")
        pickle.dump(dpt, open(fichierDpt, "wb"))
    
    dptsAyantVote += dptsVotants
    cartes(dptsAyantVote, derniersAlignements, delegues, candidats)
    print("Fini")