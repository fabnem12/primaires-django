from .BureauVote import BureauVote
from .Candidat import Candidat
from .Dpt import Dpt, listeDpts
from .Votant import Votant

from math import ceil, cos, floor, pi
from random import choice, randint
import json
import numpy as np
import os
import pickle
import requests

cheminDossierLib = os.path.dirname(os.path.abspath(__file__))
cheminData = os.path.join(os.path.dirname(os.path.dirname(cheminDossierLib)), "static/data/")

DIM_CARTE_DPT = 900

nomsDpts = {'49': 'Maine-et-Loire', '22': "Côtes-d'Armor", '09': 'Ariège', '54': 'Meurthe-et-Moselle', '19': 'Corrèze', '21': "Côte-d'Or", '57': 'Moselle', '2B': 'Haute-Corse', '18': 'Cher', '51': 'Marne', '93': 'Seine-Saint-Denis', '08': 'Ardennes', '63': 'Puy-de-Dôme', 'ZX': 'Saint-Martin/Saint-Barthélemy', '34': 'Hérault', '58': 'Nièvre', '78': 'Yvelines', '61': 'Orne', '07': 'Ardèche', 'ZC': 'Guyane', '2A': 'Corse-du-Sud', 'ZP': 'Polynésie française', '52': 'Haute-Marne', '79': 'Deux-Sèvres', '05': 'Hautes-Alpes', '75': 'Paris', '29': 'Finistère', '74': 'Haute-Savoie', '55': 'Meuse', '50': 'Manche', '46': 'Lot', '04': 'Alpes-de-Haute-Provence', '25': 'Doubs', '94': 'Val-de-Marne', '14': 'Calvados', 'ZN': 'Nouvelle-Calédonie', '53': 'Mayenne', '43': 'Haute-Loire', '88': 'Vosges', '15': 'Cantal', '30': 'Gard', '24': 'Dordogne', '76': 'Seine-Maritime', '92': 'Hauts-de-Seine', '48': 'Lozère', '87': 'Haute-Vienne', '23': 'Creuse', '66': 'Pyrénées-Orientales', '56': 'Morbihan', '33': 'Gironde', '59': 'Nord', '10': 'Aube', 'ZA': 'Guadeloupe', 'ZZ': 'Français établis hors de France', '37': 'Indre-et-Loire', '11': 'Aude', '42': 'Loire', 'ZD': 'La Réunion', '01': 'Ain', '83': 'Var', '28': 'Eure-et-Loir', '39': 'Jura', '69': 'Rhône', '71': 'Saône-et-Loire', '31': 'Haute-Garonne', '85': 'Vendée', '86': 'Vienne', '12': 'Aveyron', 'ZS': 'Saint-Pierre-et-Miquelon', '13': 'Bouches-du-Rhône', '36': 'Indre', '95': "Val-d'Oise", '70': 'Haute-Saône', '65': 'Hautes-Pyrénées', 'ZM': 'Mayotte', '16': 'Charente', '72': 'Sarthe', '90': 'Territoire de Belfort', '82': 'Tarn-et-Garonne', '67': 'Bas-Rhin', '73': 'Savoie', '45': 'Loiret', '68': 'Haut-Rhin', '35': 'Ille-et-Vilaine', '47': 'Lot-et-Garonne', '60': 'Oise', '91': 'Essonne', '44': 'Loire-Atlantique', '27': 'Eure', '32': 'Gers', 'ZB': 'Martinique', '02': 'Aisne', '64': 'Pyrénées-Atlantiques', '03': 'Allier', '40': 'Landes', '80': 'Somme', '84': 'Vaucluse', '38': 'Isère', '41': 'Loir-et-Cher', '77': 'Seine-et-Marne', '89': 'Yonne', '62': 'Pas-de-Calais', 'ZW': 'Wallis et Futuna', '81': 'Tarn', '26': 'Drôme', '17': 'Charente-Maritime', '06': 'Alpes-Maritimes'}

def posCandidats(returnListe = True):
    with open(os.path.join(cheminDossierLib, "candidats.txt"), "r") as f:
        content = [x.replace("\n", "") for x in f.readlines()]

    dico = dict()
    for line in content:
        nomCandidat, positionnement, couleur = line.split(":")
        positionnement = list(map(int,positionnement.split(",")))

        candidat = Candidat(nomCandidat, positionnement, couleur)
        dico[nomCandidat] = candidat

    return list(dico.values()) if returnListe else dico

def infosBureauxDpt(dptSeul=False):
    candidats = posCandidats(False)
    dpts = dict()
    compte = set()

    #les coordonnées des bureaux ne sont pas dans l'api pour Paris, mais il y a un fichier pour ça !
    coordBureauxParis = dict()
    with open(os.path.join(cheminDossierLib, "infosBureauxParis.json"), "r") as f:
        dataBureauxParis = json.load(f)["features"]

        for infos in dataBureauxParis:
            idBureau = infos["properties"]["id_bv"].split("-")
            idBureau = idBureau[0].zfill(2)+idBureau[1].zfill(2)

            coords = infos["geometry"]["coordinates"]
            coordBureauxParis[idBureau] = coords[::-1]

    dptsEnumeres = [dptSeul] if dptSeul else listeDpts
    for numDpt in dptsEnumeres:
        #pour récupérer les résultats de l'élection présidentielle de 2017 dans le département numDpt
        res = requests.get("https://public.opendatasoft.com/api/records/1.0/search/?dataset=election-presidentielle-2017-resultats-par-bureaux-de-vote-tour-1&facet=libelle_de_la_commune&facet=code_du_b_vote&facet=nom&exclude.nom=CHEMINADE&exclude.nom=ASSELINEAU&rows=10000&exclude.nom=LASSALLE&refine.code_du_departement=" + numDpt).json()["records"]
        res = [x["fields"] for x in res]

        if "ZZ" not in numDpt: #quasiment tous les départements ont des coordonnées, sauf les "Français établis hors de France", de numDpt=="ZZ"
            reqCoords = requests.get("https://public.opendatasoft.com/api/records/1.0/search/?dataset=contours-simplifies-des-departements-francais-2015&rows=1&refine.code_dept=" + numDpt).json()["records"]
            #on récupère auprès d'une autre API les coordonnées des contours du département

            if len(reqCoords) > 0:
                coords = reqCoords[0]["fields"]["geo_shape"]["coordinates"]
            else: #il se peut que le département n'aie pas de contours dans l'API (les territoires d'outre-mer), on met un contour nul par défaut
                coords = [[[[0, 0]]]]

            if str(coords)[:4] != "[[[[":
                coords = [coords]
        else:
            coords = [[[[0, 0]]]]

        dpt = Dpt(numDpt, [], coords)
        bureaux = dict()
        resultats = dict()
        for infos in res: #l'api est nulle : il y a une ligne par bureau de vote par candidat, on doit faire avec !
            numBureau = infos["code_de_la_commune"] + "_" + infos["code_du_b_vote"]
            nomBureau = infos["libelle_de_la_commune"] + " " + infos["code_du_b_vote"]
            candidat, voix = infos["nom"], infos["voix_exp"] #on identifie le candidat et le nombre de voix dans la ligne en cours

            if dpt.nom == "": dpt.nom = infos["libelle_du_departement"] #on identifie le nom du département, vu qu'on l'a cherché par son numéro, sans son nom

            nbVotants = infos["exprimes"] // 10 #on divise par 10 le nb d'électeurs pour aller plus vite au moment de calculer
            if nbVotants == 0: continue

            if numBureau not in bureaux: #c'est la première ligne à propos de son bureau de vote, on réquisitionne quelques infos sur le bureau
                nbRepresentants = 1 + round(nbVotants * 5 / 100) #1 + 5 représentants pour 100 votants

                bureau = BureauVote(numBureau, nomBureau, nbVotants, nbRepresentants)
                dpt.bureaux.append(bureau)
                bureaux[numBureau] = bureau
                resultats[bureau] = dict()
            else:
                bureau = bureaux[numBureau]

            if bureau.coord == [0, 0] or bureau.coord == []: #l'api est mesquine, pour certains bureaux, il n'y a pas leurs coordonnées sur toutes leurs lignes...
                if "coordonnees" in infos:
                    bureau.coord = infos["coordonnees"]
                elif numDpt == "75": #l'api de résultats ne connaît pas les coordonnées des bureaux à Paris, mais il y en a une autre traitée plus haut qui les connaît
                    bureau.coord = coordBureauxParis[infos["code_du_b_vote"]]
                else: #aucune trace des coordonnées du bureau de vote :snif:
                    bureau.coord = [0, 0]
                    compte.add(bureau)

            resultats[bureau][candidat] = voix

        for bureau in list(resultats):
            votes = resultats[bureau]
            pos = [0 for _ in range(11)]

            if sum(votes.values()) == 0:
                del bureaux[bureau]
                del resultats[bureaux]
                continue

            for nomCandidat in votes:
                candidat = candidats[nomCandidat]

                for index in range(11):
                    pos[index] += candidat.positionnement[index] * (votes[nomCandidat] / sum(votes.values()))

            bureau.pos = [round(100 * x / sum(pos)) for x in pos]

        dpt.pop = sum([x.votants for x in dpt])
        dpts[numDpt] = dpt


    #on répartit proportionnellement les délégués entre les dpts en posant que chaque dpt a au moins un délégué et qu'il y a 999 délégués au total
    nbVotants = sum([dpt.pop for dpt in dpts.values()])
    nbDeleguesFrance = 999

    nbDeleguesARepartir = nbDeleguesFrance - len(dpts) #chaque dpt démarre avec 1 délégué
    for _ in range(nbDeleguesARepartir):
        moyMax = 0
        dptMax = None

        for dpt in dpts.values():
            moy = dpt.pop / (dpt.nbDelegues + 1)
            if moy > moyMax:
                moyMax = moy
                dptMax = dpt

        dptMax.nbDelegues += 1 #le dpt avec la meilleure moyenne reçoit un nouveau délégué

    if not dptSeul:
        pickle.dump(dpts, open(os.path.join(cheminDossierLib, "dpts.p"), "wb"))
    else:
        return dpts[dptSeul]

def calendrierAlea(nbCreneaux = 20):
    if nbCreneaux <= 0: raise ValueError("Il faut plus de créneaux de vote !")
    if nbCreneaux == 1: return [listeDpts.copy()]

    premier = choice(listeDpts)
    nbCreneaux -= 1

    creneaux = [[] for _ in range(nbCreneaux)]
    while min([len(x) for x in creneaux]) == 0:
        creneaux = [[] for _ in range(nbCreneaux)]
        nbs = dict()

        for numDpt in listeDpts:
            if numDpt == premier: continue

            nbs[numDpt] = round(np.random.normal(nbCreneaux / 2, nbCreneaux / 4))
            if nbs[numDpt] < 0: nbs[numDpt] = 0
            if nbs[numDpt] >= nbCreneaux: nbs[numDpt] = nbCreneaux - 1

        for numDpt, creneau in nbs.items():
            creneaux[creneau].append(numDpt)

    return [[premier]] + creneaux

#scores doit être une liste TRIÉE par ordre décroissant de votes de tuples : [(candidat1, nbVotes1), (candidat2, nbVotes2)].
#la fonction renvoie un tuple : le code SVG correspondant sous forme d'un unique str, et la position en y du bas du recap
#candidatsBarres doit être une liste de candidats dont le nom doit être barré
def recapVotesSVG(votes, xGauche, yHaut, candidatsBarres = []):
    if votes == []: return "", yHaut
    nbVotesMax = votes[0][1]
    nbVotesTot = sum([x[1] for x in votes])
    nbEspaces = len(str(nbVotesMax))

    svgStr = ""
    for index, (candidat, nbVotes) in enumerate(votes):
        pourcentage = 0 if nbVotesTot == 0 else round(100 * nbVotes / nbVotesTot, 2)
        barre = "text-decoration:line-through;" if candidat in candidatsBarres else ""
        voixStr = str(nbVotes).zfill(nbEspaces)

        #rectangle de couleur
        svgStr += "\t<rect x='{x}' y='{y}' width='12' height='12' fill='{couleur}' />\n".format(x=xGauche, y=yHaut+17*index, couleur=candidat.couleur)
        #nom du candidat
        svgStr += "\t<text x='{x}' y='{y}' style='font-size:12;{barre}'>{candidat}</text>\n".format(x=xGauche+20, y=yHaut+17*index + 10, barre=barre, candidat=candidat)
        #nb votes du candidat et pourcentage
        svgStr += "\t<text x='{x}' y='{y}' style='font-size:12;{barre}'>{nbVotes} {pourcentage} %</text>\n".format(x=xGauche+150, y=yHaut+17*index+10, barre=barre, nbVotes=voixStr, pourcentage=pourcentage)

    return svgStr, yHaut + 30 + 17*len(votes)

def hsv2rgb(hsv):
    h, s, v = hsv

    c = v * s
    hP = h/60
    x = c * (1 - abs(hP % 2 - 1))

    cas = {
        0: (c, x, 0),
        1: (c, x, 0),
        2: (x, c, 0),
        3: (0, c, x),
        4: (0, x, c),
        5: (x, 0, c),
        6: (c, 0, x)
    }

    r1, g1, b1 = cas[ceil(hP)]
    m = v - c

    r, g, b = map(lambda x: round(255 * (x + m)), (r1, g1, b1))
    lettres = "0123456789ABCDEF"

    return "#" + "".join([lettres[x//16]+lettres[x%16] for x in (r, g, b)])

def couleursCalendrier(calendrier):
    nbCouleurs = len(calendrier)
    nbLumis = ceil(nbCouleurs / 9)

    nbCouleursLumi = ceil(nbCouleurs / nbLumis)
    couleurs = []
    lumi = 0
    i = 0
    for tour in range(nbCouleurs):
        lumi += 1
        if lumi == nbLumis:
            i += 18 / nbCouleursLumi
            lumi = 0

        couleurs.append((i*18, 1, (1-lumi/(nbLumis+1))))

    dptsParCouleur = dict()
    couleursTours = dict()
    dpts = pickle.load(open(os.path.join(cheminDossierLib, "dpts.p"), "rb"))
    for i, couleur in enumerate(couleurs):
        couleur = hsv2rgb(couleur)

        candidatCouleur = Candidat("Tour {}".format(i+1), [100], couleur) #on fait un candidat bidon pour faire office de couleur, comme ça on réutilise le code utilisé pour colorer les dpts par gagnant

        dptsParCouleur[candidatCouleur] = [dpts[x] for x in calendrier[i]]
        couleursTours[i+1] = couleur

    return dptsParCouleur, couleursTours

#carte pour afficher les résultats pour toute la France
#calendrier sert à afficher la carte du calendrier, indépendemment des dptsAyantVote
def cartes(dptsAyantVote, candidats, calendrier = []):
    #préparation de la génération du svg. les lignes sont insérées dans la liste affiClassement
    affiClassement = []

    if calendrier == []:
        #on génère les dicos récapitulatifs des derniersAlignements et délégués au niveau national (des dpts ayant vote)
        derniersAlignements = dict()
        for dpt in dptsAyantVote:
            for candidat, nbVotes in dpt.dernierAlignement.items():
                if candidat not in derniersAlignements: derniersAlignements[candidat] = 0
                derniersAlignements[candidat] += nbVotes

        delegues = dict()
        for dpt in dptsAyantVote:
            for candidat, nbDel in dpt.delegues.items():
                if candidat not in delegues: delegues[candidat] = 0
                delegues[candidat] += nbDel


        candidatsRetires = [x for x in derniersAlignements if x not in candidats]

        #affichage des derniers alignements
        if derniersAlignements != dict(): #pas la peine d'afficher des derniers alignements fantômes
            affiClassement.append('\t<text x="500" y="50" style="font-weight:bold;">Derniers alignements</text>')

        classement = sorted(derniersAlignements, key=lambda x: derniersAlignements[x], reverse=True)
        recapVotes = [(candidat, derniersAlignements[candidat]) for candidat in classement]
        recapSvg, posY = recapVotesSVG(recapVotes, 500, 70, candidatsRetires)

        affiClassement.append(recapSvg)


        #affichage des délégués
        if delegues != dict(): #pas la peine d'afficher des délégués fantômes
            affiClassement.append("\t<text x='500' y='{}' style='font-weight:bold;'>Délégués</text>\n".format(posY))

        classementRepr = sorted(delegues, key=lambda x: (delegues[x], derniersAlignements[x]), reverse=True)
        recapDelegues = [(candidat, delegues[candidat]) for candidat in classementRepr]
        recapSvg, posY = recapVotesSVG(recapDelegues, 500, posY+20, candidatsRetires)

        affiClassement.append(recapSvg)

        #on voit qui a gagné quel département
        dptsGagnesPar = dict()
        for candidat in classement: dptsGagnesPar[candidat] = []

        for dpt in dptsAyantVote:
            dptsGagnesPar[dpt.gagnant()].append(dpt)

    else: #affichage des tours de vote
        if type(calendrier[0]) != list: calendrier = [calendrier]
        dptsGagnesPar, couleursTours = couleursCalendrier(calendrier)

        affiClassement.append("\t<text x='500' y='50' style='font-weight:bold;'>{}</text>\n".format("Calendrier" if len(calendrier) != 1 else "Votent ce tour :"))

        if len(calendrier) != 1:
            for idTour, couleur in couleursTours.items():
                affiClassement.append("\t<rect x='500' y='{y}' width='12' height='12' fill='{couleur}' />\n".format(y = 50 + idTour*17, couleur = couleur))
                affiClassement.append("\t<text x='520' y ='{y}' style='font-size:12;'>Tour n°{nb}</text>\n".format(y = 60 + idTour*17, nb=idTour))
        else:
            couleur = list(dptsGagnesPar.keys())[0]

            for idDpt, dpt in enumerate(dptsGagnesPar[couleur]):
                affiClassement.append("\t<text x='500' y='{y}' style='font-size:12;'>{nomDpt}</text>\n".format(y = 67 + idDpt * 17, nomDpt = dpt.nom))

    #on retrouve le fichier svg et on ajoute les couleurs et les résultats
    with open(os.path.join(cheminDossierLib, "France.svg"), "r") as f:
        lignes = f.readlines()

    lignes = lignes[:20] + affiClassement + lignes[20:]

    for gagnant, dpts in dptsGagnesPar.items():
        if len(dpts) == 0: continue

        couleur = gagnant.couleur
        lignes.insert(13, ", ".join(["[id='{}']".format(dpt.num) for dpt in dpts]) + " {{fill: {};}}\n".format(couleur))

    return "".join(lignes)

def carte2(derniersAlignementsParDpt, deleguesParDpt, dptsParGagnants, candidats):
    """
    Dessiner une carte qui fait le résumé des résultats :
    - affiche le gagnant de chaque département
    - le total de derniers alignements et de délégués pour les départements ayant voté
    """

    codeSvg = [""]
    def printF(*args):
        codeSvg[0] += " ".join([str(x) for x in args]) + "\n"

    #initialisation du svg
    #-on récupère le fichier de base avec tous les tracés des départements
    with open(os.path.join(cheminDossierLib, "France.svg"), "r") as f:
        lignes = list(f.readlines())
    enTete, resteCode = lignes[:13], lignes[13:-1] #on garde tout sauf la fin de la balise svg, pour rajouter le résumé des alignements juste avant

    printF(*enTete)

    #on détermine la couleur de chaque dpt en fonction de son gagnant
    #pour ensuite modifier le css en conséquence
    for candidat, numDptsGagnes in dptsParGagnants.items():
        #on ne peut pas utiliser le numDpt comme nom d'id comme ça, parce que l'id n'est pas censé commencer par un nombre
        #[id='24'] permet de contourner ça
        #fill: {couleur}; pour que le département s'affiche de la couleur de son vainqueur
        printF(", ".join(["[id='{}']".format(x) for x in numDptsGagnes]), "{{fill: {couleur};}}".format(couleur=candidat.couleur))

    printF(*resteCode)

    #maintenant affichage du résumé des derniers alignements et délégués déjà attribués
    printF('<g>')

    #il faut d'abord rassembler les résultats, qui sont fournis par dpt, à tous les dpts ayant voté
    derniersAlignements = dict()
    for numDpt, dernierAlignementDpt in derniersAlignementsParDpt.items():
        for candidat, nbVotes in dernierAlignementDpt.items():
            if candidat not in derniersAlignements: derniersAlignements[candidat] = 0

            derniersAlignements[candidat] += nbVotes

    delegues = dict()
    for numDpt, deleguesDpt in deleguesParDpt.items():
        for candidat, nbDel in deleguesDpt.items():
            if candidat not in delegues: delegues[candidat] = 0

            delegues[candidat] += nbDel

    #on regarde quels sont les candidats qui se sont retirés, càd ceux qui ne sont pas dans la liste des candidats passée en argument
    #mais qui ont des derniers alignements (donc on participé au vote d'au moins un dpt)
    candidatsRetires = [x for x in derniersAlignements.keys() if x not in candidats]

    #on affiche les derniersAlignements
    printF('\t<text x="500" y="20" style="text-decoration:underline;font-size:20;">Derniers alignements</text>')

    derniersAlignementsTries = sorted(derniersAlignements.items(), key=lambda x: x[1], reverse=True)
    svgDerniersAlignements, posY = recapVotesSVG(derniersAlignementsTries, 500, 40, candidatsRetires)
    printF(svgDerniersAlignements)

    #les délégués
    printF('\t<text x="500" y="{}" style="text-decoration:underline;font-size:20;">Délégués</text>'.format(posY))

    deleguesTries = sorted(delegues.items(), key=lambda x: (x[1], derniersAlignements[x[0]]), reverse=True)
    svgDelegues, _ = recapVotesSVG(deleguesTries, 500, posY+20, candidatsRetires)
    printF(svgDelegues)

    printF("</g>")
    printF("</svg>")

    return codeSvg[0]

def GPS2SVG(coordGPS, coins, deltas, dims): #conversion des coordonnées GPS en coordonnées dans le SVG
    longi, lat = coordGPS
    coinSupGauche, coinInfDroite = coins
    deltaLonCarte, deltaLatCarte = deltas
    largeurCarte, hauteurCarte = dims

    x = ((longi - coinSupGauche[0]) / deltaLonCarte) * largeurCarte
    y = ((coinSupGauche[1] - lat) / deltaLatCarte) * hauteurCarte

    return list(map(lambda nb: round(nb+randint(-5,5)/2, 3), (x, y)))


def carteDpt(dpt, dossierPerso):
    codeSvg = [""]

    def printF(*args):
        codeSvg[0] += " ".join([str(x) for x in args]) + "\n"

    #préparation à la conversion des coordonnées GPS en coordonnées SVG
    coinSupGauche = [10**99, -10**99]
    coinInfDroite = [-10**99, 10**99]

    for gpe in dpt.coords:
        for path in gpe:
            try:
                for longi, lat in path:
                    if longi < coinSupGauche[0]: coinSupGauche[0] = longi
                    if longi > coinInfDroite[0]: coinInfDroite[0] = longi

                    if lat > coinSupGauche[1]: coinSupGauche[1] = lat
                    if lat < coinInfDroite[1]: coinInfDroite[1] = lat
            except:
                coinSupGauche = [1, -1]
                coinInfDroite = [-1, 1]


    margeX = 0.05 * (coinInfDroite[0] - coinSupGauche[0])
    margeY = 0.05 * (coinSupGauche[1] - coinInfDroite[1])
    pointHautGauche = [coinSupGauche[0] - margeX, coinSupGauche[1] + margeY]
    pointBasDroite = [coinInfDroite[0] + margeX, coinInfDroite[1] - margeY]

    deltaLatCarte = coinSupGauche[1] - coinInfDroite[1]
    deltaLonCarte = coinInfDroite[0] - coinSupGauche[0]

    hauteurCarte = deltaLatCarte * (DIM_CARTE_DPT / deltaLonCarte) / cos(pi * (pointHautGauche[1]-pointBasDroite[1]) / 360)
    largeurCarte = DIM_CARTE_DPT
    coins = [coinSupGauche, coinInfDroite]
    deltas = [deltaLonCarte, deltaLatCarte]

    dims = [largeurCarte, hauteurCarte]

    convGPS = lambda longi, lat: GPS2SVG((longi, lat), coins, deltas, dims)

    #initialisation du svg
    printF('<?xml version="1.0" encoding="utf-8"?>')
    printF('<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">')
    printF('<?xml-stylesheet type="text/css" href="http://fonts.googleapis.com/css?family=Roboto-Mono:300,400,500,700" ?>')
    printF('<svg version="1.1"')
    printF('	 id="svg2" inkscape:version="0.91 r13725" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"')
    printF('	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="1200px" height="{}px"'.format(hauteurCarte))
    printF('	 viewBox="0 0 1200 {hauteur}" enable-background="new 0 0 1200 {hauteur}" xml:space="preserve">'.format(hauteur=max(hauteurCarte, 600)))

    #CSS
    printF('<defs>')
    printF('\t<style type="text/css">')

    #Roboto Mono
    printF('\t\t@import url(http://fonts.googleapis.com/css?family=Roboto+Mono);')
    printF('\t\ttext {font-family: "Roboto Mono";}')

    #couleurs des candidats
    for candidat in dpt.premierAlignement:
        nomCandidat = candidat.nom.replace(" ", "_") #on remplace les espaces par des underscores pour avoir des noms de class valides
        printF('\t\t.{nom} {{fill: {couleur};}}'.format(nom=nomCandidat, couleur=candidat.couleur))

    printF('\t</style>')
    printF('</defs>')

    printF()

    #dessin des contours du dpt
    if dpt.coords != [[[[]]]]:
        for gpe in dpt.coords:
            printF('<g>')

            for path in gpe:
                printF('\t<polygon points="{}" style="fill:none;stroke:#000;stroke-width:0.5;" />'.format(" ".join(["{},{}".format(*convGPS(longi, lat)) for longi, lat in path])))

            printF('</g>')

    printF()
    #on dessine les points et les résultats détaillés pour chaque bureau de vote
    for bureau in dpt:
        if bureau.coord == [0, 0] or bureau.dernierAlignement == dict(): continue

        premierAlignement = sorted([(candidat, nbVoix) for candidat, nbVoix in bureau.premierAlignement.items()], key=lambda x: x[1], reverse=True)
        dernierAlignement = sorted([(candidat, nbVoix) for candidat, nbVoix in bureau.dernierAlignement.items()], key=lambda x: x[1], reverse=True)
        representants = sorted([(candidat, nbVoix) for candidat, nbVoix in bureau.representants.items()], key=lambda x: (x[1], bureau.dernierAlignement[x[0]]), reverse=True)
        gagnant = dernierAlignement[0][0]

        #point coloré du bureau
        coordsPoint = convGPS(bureau.coord[1], bureau.coord[0])
        nomGagnant = gagnant.nom.replace(" ","_") #on fait des noms de classe valides en remplaçant les espaces par des underscore (typiquement LE PEN -> LE_PEN)

        printF('<circle cx="{}" cy="{}" r="5" class="{nomGagnant}" id="{idBureau}">'.format(coordsPoint[0], coordsPoint[1], nomGagnant=nomGagnant, idBureau=bureau.num))
        printF('\t<title>{nomBureau}</title>'.format(nomBureau=bureau.nom))
        printF('</circle>')

        #résultats détaillés
        printF('<g visibility="hidden">')

        #on détermine la hauteur du bloc de résultats, afin de pouvoir l'aligner avec le point du bureau de vote sans déborder par le bas
        hauteurBloc = 0
        _, hauteurBloc = recapVotesSVG(premierAlignement, 0, hauteurBloc+40)
        _, hauteurBloc = recapVotesSVG(dernierAlignement, 0, hauteurBloc+40)
        _, hauteurBloc = recapVotesSVG(representants, 0, hauteurBloc+40)

        #on se débrouille pour que le bloc ne dépasse ni par le haut ni par le bas
        posY = max(0, min(coordsPoint[1]-hauteurBloc/2, hauteurCarte-hauteurBloc))

        printF('\t<text x="{}" y="{}" style="text-decoration:underline;">{nomBureau}</text>'.format(DIM_CARTE_DPT+20, posY+20, nomBureau=bureau.nom))
        printF('\t<text x="{}" y="{}" style="font-weight:bold;">Premier alignement</text>'.format(DIM_CARTE_DPT+20, posY+50))
        recapSvg, posY = recapVotesSVG(premierAlignement, DIM_CARTE_DPT+20, posY+70)
        printF(recapSvg)

        posY += 20
        printF('\t<text x="{}" y="{}" style="font-weight:bold;">Dernier alignement</text>'.format(DIM_CARTE_DPT+20, posY))
        recapSvg, posY = recapVotesSVG(dernierAlignement, DIM_CARTE_DPT+20, posY+20)
        printF(recapSvg)

        posY += 20
        printF('\t<text x="{}" y="{}" style="font-weight:bold;">Représentants</text>'.format(DIM_CARTE_DPT+20, posY))
        recapSvg, posY = recapVotesSVG(representants, DIM_CARTE_DPT+20, posY+20)
        printF(recapSvg)

        printF('\t<set attributeName="visibility" to="visible" begin="{idBureau}.mouseover" />'.format(idBureau=bureau.num))
        printF('\t<set attributeName="visibility" to="hidden" begin="{idBureau}.mouseout" />'.format(idBureau=bureau.num))

        printF('</g>')

    #affichage des résultats globaux du dpt
    dernierAlignement = sorted([(candidat, nbVoix) for candidat, nbVoix in dpt.dernierAlignement.items()], key=lambda x: x[1], reverse=True)
    representants = sorted([(candidat, nbRepr) for candidat, nbRepr in dpt.representants.items()], key=lambda x: (x[1], dpt.dernierAlignement[x[0]]), reverse=True)
    delegues = sorted([(candidat, nbDel) for candidat, nbDel in dpt.delegues.items()], key=lambda x: (x[1], dpt.dernierAlignement[x[0]]), reverse=True)

    printF('<g>')

    printF('\t<text x="{}" y="{}" style="text-decoration:underline;">{nomDpt} ({numDpt})</text>'.format(DIM_CARTE_DPT+20, 20, nomDpt=dpt.nom, numDpt=dpt.num))
    printF('\t<text x="{}" y="{}" style="font-weight:bold;">Dernier alignement</text>'.format(DIM_CARTE_DPT+20, 50))
    recapSvg, posY = recapVotesSVG(dernierAlignement, DIM_CARTE_DPT+20, 70)
    printF(recapSvg)

    printF('\t<text x="{}" y="{}" style="font-weight:bold;">Représentants</text>'.format(DIM_CARTE_DPT+20, posY+20))
    recapSvg, posY = recapVotesSVG(representants, DIM_CARTE_DPT+20, posY+50)
    printF(recapSvg)

    printF('\t<text x="{}" y="{}" style="font-weight:bold;">Délégués</text>'.format(DIM_CARTE_DPT+20, posY+20))
    recapSvg, posY = recapVotesSVG(delegues, DIM_CARTE_DPT+20, posY+50)
    printF(recapSvg)

    listeBureaux = [";".join(["{}.{}".format(bureau.num, mention) for bureau in dpt]) for mention in ("mouseover", "mouseout")]
    printF('\t<set attributeName="visibility" to="hidden" begin="{}" />'.format(listeBureaux[0]))
    printF('\t<set attributeName="visibility" to="visible" begin="{}" />'.format(listeBureaux[1]))
    printF('</g>')

    printF('</svg>')

    lienSVG = os.path.join(dossierPerso, dpt.num+".svg")
    with open(lienSVG, "w") as f:
        f.write(codeSvg[0])

def derouleBureauVote(bureau):
    """
    Dessiner un svg qui permet de voir les différents alignements dans un bureau de vote.
    """

    #astuce pour modifier un str avec une fonction
    codeSvg = [""]
    def printF(*args):
        codeSvg[0] += " ".join([str(x) for x in args]) + "\n"

    HAUTEUR = 625
    RAYON_VOTANT = (326**0.5)/2 #permet de caser au pire 225 votants par candidat, ça devrait aller
    #initialisation du svg
    printF('<?xml version="1.0" encoding="utf-8"?>')
    printF('<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">')
    printF('<?xml-stylesheet type="text/css" href="http://fonts.googleapis.com/css?family=Roboto-Mono:300,400,500,700" ?>')
    printF('<svg version="1.1"')
    printF('	 id="svg2" inkscape:version="0.91 r13725" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"')
    printF('	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="1200px" height="900px"')
    printF('	 viewBox="0 0 1200 900" enable-background="new 0 0 1200 900" xml:space="preserve">')

    #CSS
    printF('<defs>')
    printF('\t<style type="text/css">')

    #Roboto Mono
    printF('\t\t@import url(http://fonts.googleapis.com/css?family=Roboto+Mono);')
    printF('\t\ttext {font-family: "Roboto Mono"; user-select: none;}')

    #couleur du quadrillage
    printF('\t\tline {stroke: #000; stroke-weigth:1;}')

    #couleurs des candidats
    candidats = list(bureau.premierAlignement)
    for candidat in candidats:
        nomCandidat = candidat.nom.replace(" ", "_") #on remplace les espaces par des underscores pour avoir des noms de class valides

        printF('\t\t.{nom} {{fill: {couleur};}}'.format(nom=nomCandidat, couleur=candidat.couleur))

    printF('\t</style>')
    printF('</defs>')

    #préparation du positionnement des votants : bloc pour soutenir chaque candidat
    nbLignes = ceil(len(candidats) / 2)
    nbColonnesVotants = 450 // (RAYON_VOTANT*2)
    if nbLignes == 0: #il n'y a aucun candidat, donc pas besoin de faire de dessin (et il faut arrêter pour ne pas faire de division par 0 au calcul de hauteurBloc)
        printF('</svg>')
        return codeSvg[0]

    #une liste avec les alignements successifs
    alignements = []

    #quadrillage pour tracer un bloc par candidat
    hauteurBloc = HAUTEUR / nbLignes
    printF('<g> <!-- quadrillage -->')
    for y in range(0, HAUTEUR+1, round(hauteurBloc)):
        printF('\t<line x1="{}" y1="{}" x2="{}" y2="{}" />'.format(0, y, 900, y))

    printF('\t<line x1="{}" y1="{}" x2="{}" y2="{}" />'.format(450, 0, 450, HAUTEUR))
    printF('</g>')

    posCandidat = lambda candidat: (450 * (candidats.index(candidat) % 2), hauteurBloc * (candidats.index(candidat) // 2))
    #on récupère le détail des votes
    _, premierAlignement, representants, toursAlignements = bureau.voteNew(candidats)

    #affichage des votants au premier alignement
    alignements = [] #alignement initial
    toursAlignements = [premierAlignement] + toursAlignements
    for idTour, tour in enumerate(toursAlignements):
        alignements.append(dict())
        elimines = [x for x in toursAlignements[idTour-1] if x not in tour]

        for index, candidat in enumerate(tour):
            alignements[idTour][candidat] = [id(votant) for votant in tour[candidat]]

    nomCandidat = lambda candidat: candidat.nom.replace(" ", "_") #on remplace les espaces dans le nom par des _ pour avoir des noms de classe css valides

    printF('<g> <!-- Votants -->')
    for candidat, votants in alignements[0].items():
        indexCandidat = candidats.index(candidat)
        posX, posY = posCandidat(candidat)

        for index, ident in enumerate(votants):
            xDansBloc, yDansBloc = index % nbColonnesVotants, index // nbColonnesVotants

            #on calcule les coordonnées dans le svg à partir de elles du votant dans le bloc
            x = RAYON_VOTANT*1.25 + posX + xDansBloc * RAYON_VOTANT * 2
            y = RAYON_VOTANT*1.25 + posY + yDansBloc * RAYON_VOTANT * 2 + 25 #pour que les cercles soient sous le nom du candidat

            visibility = "visible"
            if y > posY + hauteurBloc: #on n'affiche pas les électeurs qui dépassent de leur bloc
                tropDeVotants = True
                visibility = "hidden"

            printF('\t<circle cx="{}" cy="{}" r="{}" class="{nomCandidat}" id="{id}" visibility="{visi}"><title>Initialement : {nom}</title></circle>'.format(x, y, RAYON_VOTANT-1, nomCandidat=nomCandidat(candidat), nom=candidat.nom, id=ident, visi=visibility))

        #nom du candidat en bas de son bloc
        printF('\t<text x="{}" y="{}" style="font-size:22;" text-anchor="middle" id="nom_{}" fill="#000">{}</text>'.format(posX + 225, posY+25, nomCandidat(candidat), candidat.nom))

    printF('</g>')

    printF('<text x="{}" y="{}" style="font-size:22;text-decoration:underline;" text-anchor="middle" fill="#000">{}</text>'.format(1050, 20, bureau.nom))

    if len(alignements) > 1: #pas besoin de réalignement s'il n'y a eu qu'un seul tour !
        printF('<g> <!-- Commande de réalignement -->')

        largeurMenu = 300
        espaceEntreCommandes = largeurMenu / len(alignements)

        printF('\t<text x="{}" y="{}" style="font-size:18;text-decoration:bold;" text-anchor="middle" fill="#000">Tours d\'alignement</text>'.format(1050, 70))
        for idTour in range(len(alignements)):
            x = 900 + (idTour+0.5)*espaceEntreCommandes #+0.5 pour centrer le cercle dans sa "zone réservée", qui est un rectangle
            y = 100

            printF('\t<g id="align_{}">'.format(idTour))
            printF('\t\t<circle cx="{}" cy="{}" r="15" fill="#f00"/>'.format(x, y))
            printF('\t\t<text x="{}" y="{}" style="font-size:20;" text-anchor="middle" fill="#000">{}</text>'.format(x, y+8, idTour+1))
            printF('\t</g>')

        printF('</g>')

        printF('<g> <!-- Animations de réalignement -->')

        for idTour, alignement in enumerate(alignements):
            for candidat, votants in alignement.items():
                posX, posY = posCandidat(candidat)

                for index, ident in enumerate(votants):
                    xDansBloc, yDansBloc = index % nbColonnesVotants, index // nbColonnesVotants

                    #on calcule les coordonnées dans le svg à partir de elles du votant dans le bloc
                    x = RAYON_VOTANT*1.25 + posX + xDansBloc * RAYON_VOTANT * 2
                    y = RAYON_VOTANT*1.25 + posY + yDansBloc * RAYON_VOTANT * 2 + 25 #pour que les cercles restent sous le nom du candidat

                    if y > posY + hauteurBloc: #on cache les électeurs qui dépassent du bloc de leur candidat
                        printF('\t<animate attributeName="visibility" xlink:href="#{}" begin="align_{}.click" to="hidden" dur="0.5s" fill="freeze" />'.format(ident, idTour))
                    elif "tropDeVotants" in locals(): #il se peut que le votant "réapparaisse"
                        printF('\t<animate attributeName="visibility" xlink:href="#{}" begin="align_{}.click" to="visibility" dur="0.5s" fill="freeze" />'.format(ident, idTour))

                    printF('\t<animate attributeName="cx" xlink:href="#{}" begin="align_{}.click" to="{}" id="fin_{}" dur="1s" fill="freeze" />'.format(ident, idTour, x, idTour))
                    printF('\t<animate attributeName="cy" xlink:href="#{}" begin="align_{}.click" to="{}" dur="1s" fill="freeze" />'.format(ident, idTour, y))

        #TODO : on ajoute les animations de mise en rouge du nom des candidats éliminés

        printF('</g>')

    #affichage du récapitulatif du tour
    #bureau.premierAlignement est un dico qui à un candidat associe le NB de votants, pas les votants eux-mêmes
    _, finBlocPremierAlignement = recapVotesSVG(sorted(bureau.premierAlignement.items(), key=lambda x: x[1]), 920, 220)

    printF('<g> <!-- Total de votes par alignement -->')
    for idTour, alignement in enumerate(toursAlignements):
        printF('\t<g visibility="{}">'.format("visible" if idTour == 0 else "hidden"))
        printF('\t\t<text x="{}" y="{}" style="font-size:20;text-decoration:underline;fill:#000;" text-anchor="middle">Alignement n°{}</text>'.format(1050, 200, idTour+1))
        codeAlignement, _ = recapVotesSVG(sorted([(x[0], len(x[1])) for x in alignement.items()], key=lambda x: (x[1], True if idTour+1 == len(toursAlignements) else x[0] in toursAlignements[idTour+1]), reverse=True), 920, 220)
        printF(codeAlignement)

        printF('<set attributeName="visibility" to="visible" begin="align_{}.click" />'.format(idTour))
        printF('<set attributeName="visibility" to="hidden" begin="{}" />'.format(";".join(["align_{}.click".format(i) for i in range(len(toursAlignements)) if i != idTour])))
        printF('\t</g>')

    printF('</g>')

    #affichage du total de délégués du bureau
    printF('<g> <!-- Total de représentants -->')
    printF('\t<text x="{}" y="{}" style="font-size:20;text-decoration:underline;fill:#000;" text-anchor="middle">Représentants</text>'.format(1050, finBlocPremierAlignement))
    codeRecap, _ = recapVotesSVG(sorted(bureau.representants.items(), key=lambda x: (x[1], bureau.dernierAlignement[x[0]]), reverse=True), 920, finBlocPremierAlignement+20)
    printF(codeRecap)
    printF('</g>')

    printF('</svg>')

    return codeSvg[0]
