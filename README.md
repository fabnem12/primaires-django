# SimElec

# Aparté : Le fonctionnement des primaires aux États-Unis

# Comment lancer une simulation ?

# Comment fonctionne le simulateur ?

Avant de lancer le simulateur pour la première fois, il faut lancer la commande :
`python3 manage.py migrate`

Pour lancer le simulateur, aller à la racine du projet et lancer la commande :
`python3 manage.py runserver`

Plusieurs choses s'affichent alors dans le terminal dont une url en 127.0...
Il faut l'ouvrir dans un navigateur pour voir l'interface du simulateur.


Note sur les dépendances :
il faut avoir installé (avec pip) les paquets suivants :
- Django
- django-viewflow
- django-material
- django-frontend
- numpy

S'il y a d'autres dépendances (le programme affiche une erreur du style "package not found ..."), bah il faut installer le package en question (et svp prévenez-moi pour que je le rajoute dans cette liste !)